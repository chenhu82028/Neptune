## 获取各大分区详细列表：

## 全分区接口：

https://api.live.bilibili.com/room/v1/Area/getList


| 大分区 | 小分区 | 链接                                                         |
| :----- | ------ | ------------------------------------------------------------ |
| 娱乐   |        | https://api.live.bilibili.com/room/v1/area/getList?parent_id=1 |
| 网游   |        | http://api.live.bilibili.com/room/v1/area/getList?parent_id=2 |
| 手游   |        | http://api.live.bilibili.com/room/v1/area/getList?parent_id=3 |
| 绘画   |        | http://api.live.bilibili.com/room/v1/area/getList?parent_id=4 |
| 唱见   |        | http://api.live.bilibili.com/room/v1/area/getList?parent_id=5 |
| 单机   |        | http://api.live.bilibili.com/room/v1/area/getList?parent_id=6 |

如：

~~~json
{"code":0,"msg":"success","message":"success","data":									
 [
    {"id":"21","parent_id":"1","old_area_id":"10","name":"视频唱见","act_id":"0","pk_status":"1","hot_status":1,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/72b93ddafdf63c9f0b626ad546847a3c03c92b6f.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"207","parent_id":"1","old_area_id":"10","name":"舞见","act_id":"0","pk_status":"1","hot_status":1,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/5837fa9608fab6c1465ec29c5abecab44f7bc376.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"145","parent_id":"1","old_area_id":"6","name":"视频聊天","act_id":"0","pk_status":"1","hot_status":1,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/14a8c9c6d0a7685091db270cb523690b9e78b523.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"143","parent_id":"1","old_area_id":"2","name":"才艺","act_id":"0","pk_status":"1","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/a7a2cfad137c0db0d61aece9da2f77167466db64.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"199","parent_id":"1","old_area_id":"6","name":"虚拟主播","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/7725a45469b776ee91f2d42afca1e5711f84ac51.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"136","parent_id":"1","old_area_id":"6","name":"美食","act_id":"0","pk_status":"1","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/a3580fcae212085cb2950b82b590caeaebedda81.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"123","parent_id":"1","old_area_id":"6","name":"户外","act_id":"0","pk_status":"1","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/a97daf9b1c6d16900495fab1237d8218667920c1.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"25","parent_id":"1","old_area_id":"2","name":"手工","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/56fa9416b41b4aa3620749d16c1b1161c45dc0df.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"28","parent_id":"1","old_area_id":"6","name":"萌宠","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/b6814fc82bf62dd915e2b284e890770969c8ccd5.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"27","parent_id":"1","old_area_id":"6","name":"学习","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/d5034f128ed95acf447e903c8082c9c5b6bd7271.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"33","parent_id":"1","old_area_id":"7","name":"映评馆","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/44b85dd5ead7a02dd5d3396972811cd610793ae6.png","is_new":0,"parent_name":"娱乐","area_type":0},
     {"id":"34","parent_id":"1","old_area_id":"7","name":"音乐台","act_id":"0","pk_status":"0","hot_status":0,"lock_status":"0","pic":"https://i0.hdslb.com/bfs/vc/8537694f4fe68ab0798dd5d493d3ca5deb908088.png","is_new":0,"parent_name":"娱乐","area_type":0}
 ]
}
~~~

parent_id 对应 parentAreaId，areaId 对应列表返回的id字段的值

[https://live.bilibili.com/p/eden/area-tags?parentAreaId=1&areaId=21](https://live.bilibili.com/p/eden/area-tags?parentAreaId=1&areaId=21)

## 所有直播间接口

https://api.live.bilibili.com/room/v1/room/get_user_recommend?page=1

~~~json
}
"code": 0,
"msg": "ok",
"message": "ok",
"data": [
    {
        "area": 0,
        "areaName": "",
        "face": "https://i2.hdslb.com/bfs/face/d8d54fb18afe79e6180997b0aa056cddc351cc9f.jpg",
        "is_bn": 0,
        "is_tv": 0,
        "link": "/21713898",
        "online": 0,
        "roomid": 21713898,
        "short_id": 0,
        "stream_id": 0,
        "system_cover": "https://i0.hdslb.com/bfs/live/21713898.jpg?01201446",
        "title": "该 摇 害 得 摇",
        "uid": 128608055,
        "uname": "淡泊宁静以致远丶",
        "user_cover": "https://i0.hdslb.com/bfs/live/user_cover/015b09c7b5948aac9e9aae39dada6072a21c4202.jpg"
    }
]
}
~~~

全部直播总数接口：

https://api.live.bilibili.com/room/v1/Area/getLiveRoomCountByAreaID?areaId=0

~~~json
{
    "code": 0,
    "msg": "success",
    "message": "success",
    "data": {
        "num": 6642
    }
}
~~~

### 七日榜

https://api.live.bilibili.com/rankdb/v1/RoomRank/webSevenRank?roomid=982077&ruid=16692120

~~~json
// 20200120164831
// https://api.live.bilibili.com/rankdb/v1/RoomRank/webSevenRank?roomid=982077&ruid=16692120

{
    "code": 0,
    "msg": "OK",
    "message": "OK",
    "data": {
        "list": [
            {
                "uid": 103022133,
                "uname": "Med开",
                "face": "https://i0.hdslb.com/bfs/face/05d2487824dc51898764468078630588711b7091.jpg",
                "rank": 1,
                "score": 3243000,
                "guard_level": 2,
                "isSelf": 0
            },
            {
                "uid": 2630634,
                "uname": "佛系の熊熊",
                "face": "https://i0.hdslb.com/bfs/face/2867c686db3a2fc47b8f8502800be806fceffecf.jpg",
                "rank": 2,
                "score": 3002100,
                "guard_level": 2,
                "isSelf": 0
            },
            {
                "uid": 24856782,
                "uname": "runmsway",
                "face": "https://i2.hdslb.com/bfs/face/d592c150ee6cdcd955751f56e5dde0f2b2abbc4f.jpg",
                "rank": 3,
                "score": 2196000,
                "guard_level": 3,
                "isSelf": 0
            }
        ]
    }
}
~~~

#### 参数说明

| 参数  | 说明       |
| ----- | ---------- |
| uid   | 用户id     |
| uname | 用户名     |
| rank  | 排名       |
| score | 金瓜子数量 |
| face  | 用户头像   |



### 舰队

https://api.live.bilibili.com/xlive/app-room/v1/guardTab/topList?roomid=982077&page=2&ruid=16692120&page_size=29

~~~json
// 20200120153049
// https://api.live.bilibili.com/xlive/app-room/v1/guardTab/topList?roomid=982077&page=2&ruid=16692120&page_size=1

{
  "code": 0,
  "data": {
    "info": {
      "num": 49,
      "page": 46,
      "now": 2,
      "achievement_level": 1
    },
    "list": [
      {
        "uid": 4267426,
        "ruid": 16692120,
        "rank": 1,
        "username": "甜味kk",
        "face": "http://i0.hdslb.com/bfs/face/cd58e44deb567c050e8f46ece45af264567046e8.jpg",
        "is_alive": 0,
        "guard_level": 2,
        "guard_sub_level": 0
      }
    ],
    "top3": [
      {
        "uid": 2630634,
        "ruid": 16692120,
        "rank": 1,
        "username": "佛系の熊熊",
        "face": "http://i0.hdslb.com/bfs/face/2867c686db3a2fc47b8f8502800be806fceffecf.jpg",
        "is_alive": 1, 
        "guard_level": 2,
        "guard_sub_level": 0
      },
      {
        "uid": 24144445,
        "ruid": 16692120,
        "rank": 2,
        "username": "佛系の丫吖",
        "face": "http://i0.hdslb.com/bfs/face/950f9eaf7cb102b194836bdb7329b54d7fd5fbeb.jpg",
        "is_alive": 1,
        "guard_level": 2,
        "guard_sub_level": 0
      },
      {
        "uid": 2805924,
        "ruid": 16692120,
        "rank": 3,
        "username": "Stassika",
        "face": "http://i1.hdslb.com/bfs/face/fba307b577b0f5650504ec03781c46a5a4f1ab8c.jpg",
        "is_alive": 0,
        "guard_level": 2,
        "guard_sub_level": 0
      }
    ]
  },
  "message": "",
  "msg": ""
}
~~~

#### 参数说明

| **参数**    | **说明**             |
| ----------- | -------------------- |
| num         | 总舰数               |
| page        | 总页数               |
| now         | 当前页               |
| uid         | 用户id               |
| rank        | 排序                 |
| guard_level | 1.总督 2.提督 3.舰长 |
| is_alive    | 是否在观看           |

### 友爱社

https://api.live.bilibili.com/activity/v1/UnionFans/roomFansRank?roomid=764155&ruid=6905116

~~~json
// 20200120160119
// https://api.live.bilibili.com/activity/v1/UnionFans/roomFansRank?roomid=764155&ruid=6905116

{
  "code": 0,
  "msg": "",
  "message": "",
  "data": [
    {
      "id": 3129,
      "score": 54846,
      "rank": 1,
      "union_name": "纪米の小窝",
      "uid": "348958603",
      "uname": "纪米家的萌萌",
      "face": "https://i1.hdslb.com/bfs/face/a05d7fd66950a0631baecf2c4710f91c085af4b1.jpg",
      "days": 224,
      "fight_status": 0
    },
    {
      "id": 2026,
      "score": 112,
      "rank": 2,
      "union_name": "纪米花",
      "uid": "2768698",
      "uname": "ぃ夏ゞ忆ヾ",
      "face": "https://i2.hdslb.com/bfs/face/67129c88be9b7d1eb36cf07a362c2171a1766858.jpg",
      "days": 547,
      "fight_status": 0
    }
  ]
}
~~~

#### 参数说明

| 参数       | 说明       |
| ---------- | ---------- |
| union_name | 友爱社名称 |
| rank       | 友爱社排名 |
| uid        | 用户id     |
| uname      | 用户名     |
| days       | 应援天数   |
| face       | 用户头像   |

### 粉丝榜

https://api.live.bilibili.com/rankdb/v1/RoomRank/webMedalRank?roomid=9922197&ruid=300702024

~~~json
// 20200120175014
// https://api.live.bilibili.com/rankdb/v1/RoomRank/webMedalRank?roomid=9922197&ruid=300702024

{
  "code": 0,
  "msg": "OK",
  "message": "OK",
  "data": {
   	  "medal": {
      "status": 2
   				 },
      "own": {
      "uid": 301507852,
      "uname": "拜托了_喵大人",
      "face": "https://i0.hdslb.com/bfs/face/1889f5700a625c4c5b5a83e0cdde95bad5e6e31d.jpg",
      "target_id": 0,
      "medal_name": "",
      "level": 0,
      "color": 0,
      "rank": 0,
      "special": "",
      "isMax": 0,
      "guard_level": 0,
      "rank_text": "0"
    },
      "list": [
      {
        "uid": 301331156,
        "uname": "嘻嘻家の撒撒",
        "face": "https://i2.hdslb.com/bfs/face/0c9825901c1215ef8465a66abb050753c5b550dc.jpg",
        "rank": 1,
        "medal_name": "戒不掉",
        "level": 20,
        "color": 16752445,
        "target_id": 300702024,
        "special": "",
        "isSelf": 0,
        "guard_level": 3
      }
    ]
  }
}
~~~

#### 参数说明

| 参数       | 说明                                                         |
| ---------- | ------------------------------------------------------------ |
| uid        | 用户id                                                       |
| uname      | 用户姓名                                                     |
| rank       | 粉丝榜排名                                                   |
| medal_name | 粉丝牌名                                                     |
| level      | 粉丝牌等级                                                   |
| face       | 头像链接                                                     |
| color      | 金色：16752445 粉色：16746162 紫色：10512625 蓝色：5805790 绿色：6406234 |
| target_id  | 主播uid                                                      |



### 房管

https://api.live.bilibili.com/xlive/web-room/v1/roomAdmin/get_by_room?roomid=9922197&page_size=100

~~~json
{
  "code": 0,
  "message": "0",
  "ttl": 1,
  "data": {
    "page": {
      "page": 1,
      "page_size": 100,
      "total_page": 1,
      "total_count": 12
    },
    "data": [
      {
        "uid": 3690191,
        "uname": "断片先生skr",
        "face": "http://i2.hdslb.com/bfs/face/83a0991ae3e91fbcd7908e6f2a58ae64a525659c.jpg",
        "ctime": "2018-10-25 14:43:29"
      },
      {
        "uid": 301331156,
        "uname": "嘻嘻家の撒撒",
        "face": "http://i2.hdslb.com/bfs/face/0c9825901c1215ef8465a66abb050753c5b550dc.jpg",
        "ctime": "2018-11-12 14:06:39"
      },
      {
        "uid": 160506215,
        "uname": "铁锹突击手",
        "face": "http://i1.hdslb.com/bfs/face/66ec88f681cd22a3a3ad7d9b825688a1f01d56de.jpg",
        "ctime": "2019-04-13 13:57:09"
      },
      {
        "uid": 16063761,
        "uname": "嘻嘻家の柒ば翎佐",
        "face": "http://i0.hdslb.com/bfs/face/207fc1d70a035511fffc67b55d4705dac1373053.jpg",
        "ctime": "2019-04-20 14:43:54"
      },
      {
        "uid": 19751178,
        "uname": "新东方肄业の小阿厨",
        "face": "http://i0.hdslb.com/bfs/face/58c9df4af9b8363d883605dfd675105a7056a48e.jpg",
        "ctime": "2019-06-25 17:20:35"
      },
      {
        "uid": 30568022,
        "uname": "嘻嘻家のM酱",
        "face": "http://i2.hdslb.com/bfs/face/bb475960cc5d0042aa125abd91e004940eeb66f6.jpg",
        "ctime": "2019-06-25 17:30:47"
      },
      {
        "uid": 66008114,
        "uname": "嘻嘻家の小悟空",
        "face": "http://i2.hdslb.com/bfs/face/4c0043622e34508158a8e5abac27493a09704862.jpg",
        "ctime": "2019-07-11 13:28:51"
      },
      {
        "uid": 9078870,
        "uname": "犬君是个舔狗",
        "face": "http://i0.hdslb.com/bfs/face/828f939636842448a981a166a0703afbcc450d90.gif",
        "ctime": "2019-08-31 16:44:31"
      },
      {
        "uid": 383103,
        "uname": "暑假作业还没写完",
        "face": "http://i1.hdslb.com/bfs/face/01dc7888ba84674d6b2d887794d22392b055e4b9.jpg",
        "ctime": "2019-10-24 17:31:40"
      },
      {
        "uid": 346685845,
        "uname": "我还是叫KCC吧",
        "face": "http://i0.hdslb.com/bfs/face/f54155d719cde496fdbb32dcbb02af2e7fa82143.jpg",
        "ctime": "2019-11-12 14:16:42"
      },
      {
        "uid": 314824541,
        "uname": "嘻嘻家の贞贞小可爱",
        "face": "http://i1.hdslb.com/bfs/face/4de9c7938eb05fcc0ddc1e47ae312edc179c4f09.jpg",
        "ctime": "2019-12-29 18:11:52"
      },
      {
        "uid": 21993633,
        "uname": "老古没得感情",
        "face": "http://i0.hdslb.com/bfs/face/fe810222c00603b875184ac9865e9fe3469c0f49.jpg",
        "ctime": "2020-01-05 12:39:31"
      }
    ],
    "max_room_anchors_number": 150
  }
}
~~~

#### 参数说明

| 参数  | 说明     |
| ----- | -------- |
| uid   | 用户id   |
| uname | 用户名   |
| face  | 用户头像 |
| ctime | 任命时间 |

### 获取主播信息接口1

https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=21660412

~~~json
// 20200123142251
// https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=21660412

{
  "code": 0,
  "message": "0",
  "ttl": 1,
  "data": {
    "room_info": 
      "uid": 478304235,
      "room_id": 21660412,
      "short_id": 0,
      "title": "(祖传口胡）唱歌机器已上线~",
      "cover": "http://i0.hdslb.com/bfs/live/user_cover/ffb2ccf7cfa2b33a06eae315a2c68708367b1f3c.jpg",
      "tags": "",
      "background": "http://static.hdslb.com/live-static/images/bg/6.jpg",
      "description": "<p># 21  只是太爱你</p>",
      "live_status": 1,
      "live_start_time": 1579760407,
      "live_screen_type": 0,
      "lock_status": 0,
      "lock_time": 0,
      "hidden_status": 0,
      "hidden_time": 0,
      "area_id": 190,
      "area_name": "唱见电台",
      "parent_area_id": 5,
      "parent_area_name": "电台",
      "keyframe": "http://i0.hdslb.com/bfs/live/21660412.jpg?01230101",
      "special_type": 0,
      "up_session": "",
      "pk_status": 0,
      "is_studio": false,
      "pendants": {
        "frame": {
          "name": "鼠年初级头像框",
          "value": "https://i0.hdslb.com/bfs/vc/b132fdde54a32041e51133714604bc52f4f024d7.png",
          "desc": ""
        }
      },
      "on_voice_join": 0,
      "online": 1205,
      "room_type": {
        "4-1": 1
      }
    },
    "anchor_info": {
      "base_info": {
        "uname": "兔叽叽姐姐",
        "face": "http://i2.hdslb.com/bfs/face/859a3468d1ac4df8e2a7c915f5da8aa464b2a640.jpg",
        "gender": "保密",
        "official_info": {
          "role": -1,
          "title": "",
          "desc": ""
        }
      },
      "live_info": {
        "level": 24,
        "level_color": 10512625,
        "score": 2373036,
        "upgrade_score": 631774,
        "current": [
          620000,
          2214810
        ],
        "next": [
          790000,
          3004810
        ]
      },
      "relation_info": {
        "attention": 1878
      }
    },
    "rankdb_info": {
      "roomid": 21660412,
      "rank_desc": "小时总榜",
      "color": "#FB7299",
      "h5_url": "https://live.bilibili.com/p/html/live-app-rankcurrent/index.html?is_live_half_webview=1&hybrid_half_ui=1,5,85p,70p,FFE293,0,30,100,10;2,2,320,100p,FFE293,0,30,100,0;4,2,320,100p,FFE293,0,30,100,0;6,5,65p,60p,FFE293,0,30,100,10;5,5,55p,60p,FFE293,0,30,100,10;3,5,85p,70p,FFE293,0,30,100,10;7,5,65p,60p,FFE293,0,30,100,10;&anchor_uid=478304235&rank_type=master_realtime_hour_room&area_hour=1&area_v2_id=190&area_v2_parent_id=5",
      "web_url": "https://live.bilibili.com/blackboard/room-current-rank.html?rank_type=master_realtime_hour_room&area_hour=1&area_v2_id=190&area_v2_parent_id=5",
      "timestamp": 1579760572
    },
    "area_rank_info": {
      "areaRank": {
        "index": 4,
        "rank": ">100"
      }
    },
    "activity_init_info": {
      "eventList": [
        
      ],
      "weekInfo": {
        "bannerInfo": null,
        "giftName": null
      },
      "giftName": null,
      "lego": {
        "timestamp": 1579760572,
        "config": "[{\"name\":\"frame-mng\",\"url\":\"https:\\/\\/live.bilibili.com\\/p\\/html\\/live-web-mng\\/index.html?roomid=#roomid#&arae_id=#area_id#&parent_area_id=#parent_area_id#&ruid=#ruid#\",\"startTime\":1559544736,\"endTime\":1877167950,\"type\":\"frame-mng\"},{\"name\":\"gogogopage\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20191028\\/827e14bd59aba86e36e4e29bd3ca6b0e\\/L6Yiz6WVx.png\",\"text\":\"\\u6bcf\\u65e5\\u5386\\u9669\",\"url\":\"https:\\/\\/live.bilibili.com\\/p\\/html\\/live-app-daily\\/index.html?room_id=#roomid#&width=376&height=600\",\"color\":\"#3a2d7c\",\"startTime\":1573142400,\"endTime\":1732264659},{\"name\":\"nys-task\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20200112\\/de1d1dfd560c306a32bc79355a82fd45\\/JUCdxmxrh.png\",\"text\":\"\\u793c\\u5305\",\"url\":\"https:\\/\\/live.bilibili.com\\/activity\\/nys-2020-web\\/index.html?anchor=package#\\/page-one\",\"color\":\"#2e6fc0\",\"startTime\":1579420800,\"endTime\":1581177600},{\"name\":\"nys-rank\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20200112\\/de1d1dfd560c306a32bc79355a82fd45\\/oTZYwQBLz7.png\",\"text\":\"\\u699c\\u5355\",\"url\":\"https:\\/\\/live.bilibili.com\\/activity\\/nys-2020-room\\/index.html?room_id=#roomid#&width=376&height=600\\/#\\/detail\",\"color\":\"#2e6fc0\",\"startTime\":1579420800,\"endTime\":1581177600}]"
      }
    },
    "web_banner_info": {
      "id": 0,
      "title": "",
      "left": "",
      "right": "",
      "jump_url": "",
      "bg_color": "",
      "hover_color": "",
      "text_bg_color": "",
      "text_hover_color": "",
      "link_text": "",
      "link_color": "",
      "input_color": "",
      "input_text_color": "",
      "input_hover_color": "",
      "input_border_color": "",
      "input_search_color": ""
    },
    "lol_info": {
      "lol_activity": {
        "status": 0,
        "guess_cover": "http://i0.hdslb.com/bfs/live/61d1c4bcce470080a5408d6c03b7b48e0a0fa8d7.png",
        "vote_cover": "https://i0.hdslb.com/bfs/activity-plat/static/20190930/4ae8d4def1bbff9483154866490975c2/oWyasOpox.png",
        "vote_h5_url": "https://live.bilibili.com/p/html/live-app-wishhelp/index.html?is_live_half_webview=1&hybrid_biz=live-app-wishhelp&hybrid_rotate_d=1&hybrid_half_ui=1,3,100p,360,0c1333,0,30,100;2,2,375,100p,0c1333,0,30,100;3,3,100p,360,0c1333,0,30,100;4,2,375,100p,0c1333,0,30,100;5,3,100p,360,0c1333,0,30,100;6,2,100p,360,0c1333,0,30,100;7,3,100p,360,0c1333,0,30,100;8,3,100p,360,0c1333,0,30,100;",
        "vote_use_h5": true
      }
    },
    "wish_list_info": {
      "status": 1,
      "list": [
        {
          "id": 75697,
          "uid": 478304235,
          "type": 1,
          "type_id": 25,
          "wish_limit": 1,
          "wish_progress": 0,
          "status": 1,
          "content": "明信片",
          "ctime": "2019-12-24 13:36:54",
          "count_map": [
            1,
            3,
            5
          ]
        }
      ]
    },
    "score_card_info": null,
    "governor_show_info": {
      "cnt": 0,
      "list": [
        
      ]
    },
    "pk_info": null,
    "battle_info": null,
    "silent_room_info": null,
    "switch_info": {
      "close_guard": false,
      "close_gift": false,
      "close_online": false,
      "close_danmaku": false
    },
    "record_switch_info": {
      "record_tab": false
    },
    "room_config_info": {
      "dm_text": "发个弹幕呗~"
    }
  }
}
~~~

### 获取主播信息接口2

https://api.live.bilibili.com/room_ex/v1/RoomNews/get?roomid=21660412&uid=478304235

~~~json
// 20200123143238
// https://api.live.bilibili.com/room_ex/v1/RoomNews/get?roomid=21660412&uid=478304235

{
  "code": 0,
  "msg": "ok",
  "message": "ok",
  "data": {
    "roomid": "21660412",
    "uid": "478304235",
    "content": "每天上午九点 下午两点直播",
    "ctime": "2020-01-22 11:50:30",
    "status": "0",
    "uname": "兔叽叽姐姐"
  }
}
~~~



### 头衔接口

https://api.live.bilibili.com/rc/v1/Title/webTitles



url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAMAAADVRocKAAADAFBMV…h1Lue213NNCs7VEarPBT3XJl+gWAz4rs9/fv79mzevWP6/ATsRAQhTWDk0AAAAAElFTkSuQmCC);

## 通过房间号获取uid

https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=10998

~~~json
// 20200127120229
// https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=10998

{
  "code": 0,
  "message": "0",
  "ttl": 1,
  "data": {
    "room_info": {
      "uid": 106514,
      "room_id": 10998,
      "short_id": 1219,
      "title": "新年新气象",
      "cover": "http://i0.hdslb.com/bfs/live/room_cover/46a18ce9d64458e5bd25ab3ee12eb8a0811f48b2.jpg",
      "tags": "炉石传说,小点,起飞咯",
      "background": "http://static.hdslb.com/live-static/images/bg/1.jpg",
      "description": "<p>喜欢的话请关注 喂食 上舰支持一下~</p>\n<p>龙门阵QQ群：687941619</p>\n<p>直播歌单：网易云音乐搜好哥哥点</p>\n<p>&nbsp;</p>",
      "live_status": 0,
      "live_start_time": 0,
      "live_screen_type": 0,
      "lock_status": 0,
      "lock_time": 0,
      "hidden_status": 0,
      "hidden_time": 0,
      "area_id": 91,
      "area_name": "炉石传说",
      "parent_area_id": 2,
      "parent_area_name": "网游",
      "keyframe": "http://i0.hdslb.com/bfs/live/10998.jpg?01261701",
      "special_type": 0,
      "up_session": "",
      "pk_status": 0,
      "is_studio": false,
      "pendants": {
        "frame": {
          "name": "",
          "value": "",
          "desc": ""
        }
      },
      "on_voice_join": 0,
      "online": 2026,
      "room_type": {
        "4-1": 1
      }
    },
    "anchor_info": {
      "base_info": {
        "uname": "好哥哥小点君",
        "face": "http://i1.hdslb.com/bfs/face/5d90ef03e5e6b5f41c4d905de4472162e69faaa6.jpg",
        "gender": "男",
        "official_info": {
          "role": -1,
          "title": "",
          "desc": ""
        }
      },
      "live_info": {
        "level": 29,
        "level_color": 10512625,
        "score": 9312987,
        "upgrade_score": 2570823,
        "current": [
          2104000,
          9013810
        ],
        "next": [
          2870000,
          11883810
        ]
      },
      "relation_info": {
        "attention": 19906
      }
    },
    "rankdb_info": {
      "roomid": 10998,
      "rank_desc": "小时总榜",
      "color": "#FB7299",
      "h5_url": "https://live.bilibili.com/p/html/live-app-rankcurrent/index.html?is_live_half_webview=1&hybrid_half_ui=1,5,85p,70p,FFE293,0,30,100,10;2,2,320,100p,FFE293,0,30,100,0;4,2,320,100p,FFE293,0,30,100,0;6,5,65p,60p,FFE293,0,30,100,10;5,5,55p,60p,FFE293,0,30,100,10;3,5,85p,70p,FFE293,0,30,100,10;7,5,65p,60p,FFE293,0,30,100,10;&anchor_uid=106514&rank_type=master_realtime_hour_room&area_hour=1&area_v2_id=91&area_v2_parent_id=2",
      "web_url": "https://live.bilibili.com/blackboard/room-current-rank.html?rank_type=master_realtime_hour_room&area_hour=1&area_v2_id=91&area_v2_parent_id=2",
      "timestamp": 1580097750
    },
    "area_rank_info": {
      "areaRank": {
        "index": 0,
        "rank": ">1000"
      }
    },
    "activity_init_info": {
      "eventList": [
        
      ],
      "weekInfo": {
        "bannerInfo": null,
        "giftName": {
          "30046": {
            "gift_id": 30046,
            "uanme": "",
            "face": "http://i0.hdslb.com/bfs/face/5d94b9727a49815716cd66fc7ba3840382025c56.jpg",
            "first_uname": "ZenosYX",
            "first_face": "http://i2.hdslb.com/bfs/face/ca0a26f865734c2b44101b3b1a320c7a36702beb.jpg"
          },
          "30064": {
            "gift_id": 30064,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/d0dad8800774a4903547b1326d1fd927df47b4e9.jpg",
            "first_uname": "辣椒沫儿迷之喜欢TK",
            "first_face": "http://i1.hdslb.com/bfs/face/3a409e449704072278ff27970b689cb0c67176f6.jpg"
          },
          "30087": {
            "gift_id": 30087,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/cb08aff97a24bb6a18a46cf924229d3c3bb5960e.jpg",
            "first_uname": "此情待追忆当时却枉然",
            "first_face": "http://i0.hdslb.com/bfs/face/ea7a750ee01eae09bb7c5e8dba0f328ae8ce735a.jpg"
          },
          "30088": {
            "gift_id": 30088,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/bcdf640faa16ebaacea1d4c930baabaec9087a80.jpg",
            "first_uname": "改不回来的壁花",
            "first_face": "http://i0.hdslb.com/bfs/face/fcf95e6dbeee1a441800ff9c31d541acd51e0a61.jpg"
          },
          "30089": {
            "gift_id": 30089,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/f6624ae51b19f336c70fc108b6bda9de7bd0eae2.jpg",
            "first_uname": "綺羅-",
            "first_face": "http://i1.hdslb.com/bfs/face/792f500254dedd3bca2e8b60aa01b72aedeadc7c.jpg"
          },
          "30090": {
            "gift_id": 30090,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/b8bd2c79fe59196a5aae3aa66105b684c316e565.jpg",
            "first_uname": "489吱钵钵老母鸡",
            "first_face": "http://i0.hdslb.com/bfs/face/92f9f16ae3d226e82b924338a42c72e0e4b4ccb9.jpg"
          },
          "30091": {
            "gift_id": 30091,
            "uanme": "",
            "face": "http://i1.hdslb.com/bfs/face/0eb7ef1ac87ed643d1734ea07dd1b0da6a3084c6.jpg",
            "first_uname": "花田田的田",
            "first_face": "http://static.hdslb.com/images/member/noface.gif"
          },
          "30229": {
            "gift_id": 30229,
            "uanme": "",
            "face": "http://i2.hdslb.com/bfs/face/f6d3536c3004254174d8da34a1c261115f90e3c0.jpg",
            "first_uname": "LadonQ",
            "first_face": "http://i1.hdslb.com/bfs/face/efda10841a6a4a5b23003b7749e48cbded7fb18a.jpg"
          }
        }
      },
      "giftName": {
        "30046": {
          "gift_id": 30046,
          "uname": "EdmundDZhang",
          "face": "http://i0.hdslb.com/bfs/face/5d94b9727a49815716cd66fc7ba3840382025c56.jpg",
          "first_uname": "ZenosYX",
          "first_face": "http://i2.hdslb.com/bfs/face/ca0a26f865734c2b44101b3b1a320c7a36702beb.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30064": {
          "gift_id": 30064,
          "uname": "逍遥散人",
          "face": "http://i2.hdslb.com/bfs/face/d0dad8800774a4903547b1326d1fd927df47b4e9.jpg",
          "first_uname": "辣椒沫儿迷之喜欢TK",
          "first_face": "http://i1.hdslb.com/bfs/face/3a409e449704072278ff27970b689cb0c67176f6.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30087": {
          "gift_id": 30087,
          "uname": "正直的男U",
          "face": "http://i2.hdslb.com/bfs/face/cb08aff97a24bb6a18a46cf924229d3c3bb5960e.jpg",
          "first_uname": "此情待追忆当时却枉然",
          "first_face": "http://i0.hdslb.com/bfs/face/ea7a750ee01eae09bb7c5e8dba0f328ae8ce735a.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30088": {
          "gift_id": 30088,
          "uname": "痒局长",
          "face": "http://i2.hdslb.com/bfs/face/bcdf640faa16ebaacea1d4c930baabaec9087a80.jpg",
          "first_uname": "改不回来的壁花",
          "first_face": "http://i0.hdslb.com/bfs/face/fcf95e6dbeee1a441800ff9c31d541acd51e0a61.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30089": {
          "gift_id": 30089,
          "uname": "顾于浮生如梦",
          "face": "http://i2.hdslb.com/bfs/face/f6624ae51b19f336c70fc108b6bda9de7bd0eae2.jpg",
          "first_uname": "綺羅-",
          "first_face": "http://i1.hdslb.com/bfs/face/792f500254dedd3bca2e8b60aa01b72aedeadc7c.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30090": {
          "gift_id": 30090,
          "uname": "绝不早到小吱吱",
          "face": "http://i2.hdslb.com/bfs/face/b8bd2c79fe59196a5aae3aa66105b684c316e565.jpg",
          "first_uname": "489吱钵钵老母鸡",
          "first_face": "http://i0.hdslb.com/bfs/face/92f9f16ae3d226e82b924338a42c72e0e4b4ccb9.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30091": {
          "gift_id": 30091,
          "uname": "甜九__微微醺",
          "face": "http://i1.hdslb.com/bfs/face/0eb7ef1ac87ed643d1734ea07dd1b0da6a3084c6.jpg",
          "first_uname": "花田田的田",
          "first_face": "http://static.hdslb.com/images/member/noface.gif",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        },
        "30229": {
          "gift_id": 30229,
          "uname": "C酱です",
          "face": "http://i2.hdslb.com/bfs/face/f6d3536c3004254174d8da34a1c261115f90e3c0.jpg",
          "first_uname": "LadonQ",
          "first_face": "http://i1.hdslb.com/bfs/face/efda10841a6a4a5b23003b7749e48cbded7fb18a.jpg",
          "bg_url": "http://i0.hdslb.com/bfs/live/9eab4a0babdbbc68b884fbb885827abb4eb71da2.png",
          "text_color_1": "#FFFFFF",
          "text_color_2": "#ECCEFF"
        }
      },
      "lego": {
        "timestamp": 1580097750,
        "config": "[{\"name\":\"frame-mng\",\"url\":\"https:\\/\\/live.bilibili.com\\/p\\/html\\/live-web-mng\\/index.html?roomid=#roomid#&arae_id=#area_id#&parent_area_id=#parent_area_id#&ruid=#ruid#\",\"startTime\":1559544736,\"endTime\":1877167950,\"type\":\"frame-mng\"},{\"name\":\"gogogopage\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20191028\\/827e14bd59aba86e36e4e29bd3ca6b0e\\/L6Yiz6WVx.png\",\"text\":\"\\u6bcf\\u65e5\\u5386\\u9669\",\"url\":\"https:\\/\\/live.bilibili.com\\/p\\/html\\/live-app-daily\\/index.html?room_id=#roomid#&width=376&height=600\",\"color\":\"#3a2d7c\",\"startTime\":1573142400,\"endTime\":1732264659},{\"name\":\"nys-task\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20200112\\/de1d1dfd560c306a32bc79355a82fd45\\/JUCdxmxrh.png\",\"text\":\"\\u793c\\u5305\",\"url\":\"https:\\/\\/live.bilibili.com\\/activity\\/nys-2020-web\\/index.html?anchor=package#\\/page-one\",\"color\":\"#2e6fc0\",\"startTime\":1579420800,\"endTime\":1581177600},{\"name\":\"nys-rank\",\"target\":\"sidebar\",\"icon\":\"https:\\/\\/i0.hdslb.com\\/bfs\\/activity-plat\\/static\\/20200112\\/de1d1dfd560c306a32bc79355a82fd45\\/oTZYwQBLz7.png\",\"text\":\"\\u699c\\u5355\",\"url\":\"https:\\/\\/live.bilibili.com\\/activity\\/nys-2020-room\\/index.html?room_id=#roomid#&width=376&height=600\\/#\\/detail\",\"color\":\"#2e6fc0\",\"startTime\":1579420800,\"endTime\":1581177600}]"
      }
    },
    "web_banner_info": {
      "id": 0,
      "title": "",
      "left": "",
      "right": "",
      "jump_url": "",
      "bg_color": "",
      "hover_color": "",
      "text_bg_color": "",
      "text_hover_color": "",
      "link_text": "",
      "link_color": "",
      "input_color": "",
      "input_text_color": "",
      "input_hover_color": "",
      "input_border_color": "",
      "input_search_color": ""
    },
    "lol_info": {
      "lol_activity": {
        "status": 0,
        "guess_cover": "http://i0.hdslb.com/bfs/live/61d1c4bcce470080a5408d6c03b7b48e0a0fa8d7.png",
        "vote_cover": "https://i0.hdslb.com/bfs/activity-plat/static/20190930/4ae8d4def1bbff9483154866490975c2/oWyasOpox.png",
        "vote_h5_url": "https://live.bilibili.com/p/html/live-app-wishhelp/index.html?is_live_half_webview=1&hybrid_biz=live-app-wishhelp&hybrid_rotate_d=1&hybrid_half_ui=1,3,100p,360,0c1333,0,30,100;2,2,375,100p,0c1333,0,30,100;3,3,100p,360,0c1333,0,30,100;4,2,375,100p,0c1333,0,30,100;5,3,100p,360,0c1333,0,30,100;6,2,100p,360,0c1333,0,30,100;7,3,100p,360,0c1333,0,30,100;8,3,100p,360,0c1333,0,30,100;",
        "vote_use_h5": true
      }
    },
    "wish_list_info": {
      "status": 1,
      "list": [
        {
          "id": 2459,
          "uid": 106514,
          "type": 1,
          "type_id": 4,
          "wish_limit": 2000,
          "wish_progress": 147,
          "status": 1,
          "content": "喵娘",
          "ctime": "2018-01-13 11:24:12",
          "count_map": [
            1,
            10,
            100
          ]
        },
        {
          "id": 3111,
          "uid": 106514,
          "type": 1,
          "type_id": 8,
          "wish_limit": 2333,
          "wish_progress": 1792,
          "status": 1,
          "content": "嘲笑我吧！",
          "ctime": "2018-01-14 14:03:10",
          "count_map": [
            1,
            3,
            5
          ]
        },
        {
          "id": 7291,
          "uid": 106514,
          "type": 1,
          "type_id": 3,
          "wish_limit": 1219,
          "wish_progress": 1088,
          "status": 1,
          "content": "勋章",
          "ctime": "2018-01-24 14:52:03",
          "count_map": [
            1,
            10,
            100
          ]
        }
      ]
    },
    "score_card_info": null,
    "governor_show_info": {
      "cnt": 0,
      "list": [
        
      ]
    },
    "pk_info": null,
    "battle_info": null,
    "silent_room_info": null,
    "switch_info": {
      "close_guard": false,
      "close_gift": false,
      "close_online": false,
      "close_danmaku": false
    },
    "record_switch_info": {
      "record_tab": false
    },
    "room_config_info": {
      "dm_text": "发个弹幕呗~"
    }
  }
}
~~~

## 六大分区详细接口

### 网游

https://api.live.bilibili.com/room/v3/area/getRoomList?parent_area_id=2&cate_id=0&area_id=0&sort_type=sort_type_124&page=1&page_size=2

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 7586,
    "list": [
      {
        "roomid": 7734200,
        "uid": 50329118,
        "title": "JDG vs EDG 2020LPL夏季赛",
        "uname": "哔哩哔哩英雄联盟赛事",
        "online": 18039061,
        "user_cover": "https://i0.hdslb.com/bfs/vc/83dd17159f92ddfe7edc8dbebb6621740503adaa.png",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070522410000077342007dauc5.jpg",
        "cover": "https://i0.hdslb.com/bfs/vc/83dd17159f92ddfe7edc8dbebb6621740503adaa.png",
        "show_cover": "",
        "link": "/6",
        "face": "https://i0.hdslb.com/bfs/face/cb620bbb9071974f37843134875d472b47532a97.jpg",
        "parent_id": 2,
        "parent_name": "网游",
        "area_id": 86,
        "area_name": "英雄联盟",
        "area_v2_parent_id": 2,
        "area_v2_parent_name": "网游",
        "area_v2_id": 86,
        "area_v2_name": "英雄联盟",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": [
          
        ],
        "verify": {
          "role": 3,
          "desc": "哔哩哔哩英雄联盟赛事直播官方账号",
          "type": 1
        },
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "86F5FCD3-EFF1-A73A-26F0-FBE7651620CA",
        "group_id": 0
      },
      {
        "roomid": 35399,
        "uid": 4931952,
        "title": "旅店开门晚了！",
        "uname": "奶粉の日常",
        "online": 340808,
        "user_cover": "https://i0.hdslb.com/bfs/live/room_cover/fdd291f62666a6a402ec711bbfbeeee14371162c.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe07062041000000035399o458m2.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/room_cover/fdd291f62666a6a402ec711bbfbeeee14371162c.jpg",
        "show_cover": "",
        "link": "/1150",
        "face": "https://i2.hdslb.com/bfs/face/6a5ce833d98ee0861217a62937c9dab514ba64b9.jpg",
        "parent_id": 2,
        "parent_name": "网游",
        "area_id": 91,
        "area_name": "炉石传说",
        "area_v2_parent_id": 2,
        "area_v2_parent_name": "网游",
        "area_v2_id": 91,
        "area_v2_name": "炉石传说",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": [
          
        ],
        "verify": {
          "role": 1,
          "desc": "bilibili 知名游戏UP主、直播签约主播",
          "type": 0
        },
        "head_box": {
          "name": "“粽叶飘香”头像框",
          "value": "https://i0.hdslb.com/bfs/vc/da2497540c5dc6a7f555f8d255f8ba2bc5221852.png"
        },
        "head_box_type": 1,
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "EEDFE247-20FA-8107-0812-60C136FF97E5",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 40158,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 1,
        "link": "https://www.bilibili.com/blackboard/live/activity-tcGTd17UO.html"
      },
      {
        "id": 36584,
        "pic": "https://i0.hdslb.com/bfs/live/49bffc5a41c213b04c09ca91495735d81fdc56d5.jpg",
        "title": "网游分区月度榜单",
        "position": 2,
        "link": "https://www.bilibili.com/blackboard/live/activity-K3sr0LPAg.html"
      },
      {
        "id": 39968,
        "pic": "https://i0.hdslb.com/bfs/live/7dd1a48cc30f1878adc2a7208efbdab37d8ef0ca.jpg",
        "title": "来B站直播冲榜LOL 赢百万现金大奖",
        "position": 3,
        "link": "https://www.bilibili.com/blackboard/live/activity-miMXuv-9X.html"
      },
      {
        "id": 40036,
        "pic": "https://i0.hdslb.com/bfs/live/36b832efd109467c332d1763c700fc409f85c1a5.jpg",
        "title": "2020 英雄联盟职业联赛夏季赛",
        "position": 4,
        "link": "https://live.bilibili.com/7734200?broadcast_type=0"
      }
    ],
    "tags": [
      {
        "id": 124,
        "name": "热门",
        "sort": "1",
        "sort_type": "sort_type_124"
      },
      {
        "id": 14,
        "name": "最新",
        "sort": "2",
        "sort_type": "live_time"
      },
      {
        "id": 58,
        "name": "萌新",
        "sort": "3",
        "sort_type": "sort_type_58"
      }
    ],
    "new_tags": [
      {
        "id": 124,
        "name": "热门",
        "icon": "",
        "sort_type": "sort_type_124",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 14,
        "name": "最新",
        "icon": "",
        "sort_type": "live_time",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 58,
        "name": "萌新",
        "icon": "",
        "sort_type": "sort_type_58",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~

### 手游

https://api.live.bilibili.com/room/v3/area/getRoomList?parent_area_id=3&cate_id=0&area_id=0&sort_type=sort_type_121&page=1&page_size=30&tag_version=1

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 3668,
    "list": [
      {
        "roomid": 90713,
        "uid": 7946235,
        "title": "2100分巅峰赛来啦！",
        "uname": "顾于浮生如梦",
        "online": 1100940,
        "user_cover": "https://i0.hdslb.com/bfs/live/user_cover/de2f8e3269b2ff510e2584159be8aca58fbe848f.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe0706211000000009071347aedj.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/user_cover/de2f8e3269b2ff510e2584159be8aca58fbe848f.jpg",
        "show_cover": "",
        "link": "/469",
        "face": "https://i0.hdslb.com/bfs/face/f6624ae51b19f336c70fc108b6bda9de7bd0eae2.jpg",
        "parent_id": 3,
        "parent_name": "手游",
        "area_id": 35,
        "area_name": "王者荣耀",
        "area_v2_parent_id": 3,
        "area_v2_parent_name": "手游",
        "area_v2_id": 35,
        "area_v2_name": "王者荣耀",
        "web_pendent": "lottery_anchor_draw",
        "pk_id": 0,
        "pendant_info": {
          "1": {
            "content": "",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/539ce26c45cd4019f55b64cfbcedc3c01820e539.png",
            "position": 1,
            "type": "mobile_index_badge",
            "name": "百人成就"
          },
          "2": {
            "content": "天选时刻",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/0cc6f3244063981b21cec7be2692d85f1a6d7792.png",
            "position": 2,
            "type": "mobile_index_badge",
            "name": "lottery_anchor_draw"
          }
        },
        "verify": {
          "role": 2,
          "desc": "bilibil直播签约主播",
          "type": 0
        },
        "head_box": {
          "name": "周星star头像框",
          "value": "https://i0.hdslb.com/bfs/vc/91eb4828c6ff3c27e8d6de5c6fe71dcb40fcf0a9.png"
        },
        "head_box_type": 1,
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "A766002E-1B57-92D3-9079-41BF3E97F049",
        "group_id": 0
      },
      {
        "roomid": 3742025,
        "uid": 93841264,
        "title": "大娱乐家赵二狗啾咪",
        "uname": "梦醒三生梦",
        "online": 1076032,
        "user_cover": "https://i0.hdslb.com/bfs/live/c15e2affba501706a882e8727cecf44e05ac7f50.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe07062111000003742025gycvl5.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/c15e2affba501706a882e8727cecf44e05ac7f50.jpg",
        "show_cover": "",
        "link": "/439",
        "face": "https://i2.hdslb.com/bfs/face/1ad8c4948eb6c0753dfdefe50dc76f2ba7d97840.jpg",
        "parent_id": 3,
        "parent_name": "手游",
        "area_id": 35,
        "area_name": "王者荣耀",
        "area_v2_parent_id": 3,
        "area_v2_parent_name": "手游",
        "area_v2_id": 35,
        "area_v2_name": "王者荣耀",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": {
          "1": {
            "content": "",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/539ce26c45cd4019f55b64cfbcedc3c01820e539.png",
            "position": 1,
            "type": "mobile_index_badge",
            "name": "百人成就"
          }
        },
        "verify": {
          "role": 2,
          "desc": "bilibili直播签约主播",
          "type": 0
        },
        "head_box_type": 1,
        "head_box": {
          "name": "Star星耀",
          "value": "https://i0.hdslb.com/bfs/vc/8b8e483ae46ef634e9babe3c13b9dbb99f374c38.png"
        },
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "55666C08-4587-BCD6-AB76-C5891DF92674",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 40160,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 2,
        "link": "https://www.bilibili.com/blackboard/live/activity-tcGTd17UO.html"
      },
      {
        "id": 40211,
        "pic": "https://i0.hdslb.com/bfs/live/7aa9bb689b6f309292ab46b8ffeb2c39b25319ca.jpg",
        "title": "猫和老鼠粽在一起",
        "position": 3,
        "link": "https://www.bilibili.com/blackboard/live/activity-NI0QLB3oE.html"
      },
      {
        "id": 39889,
        "pic": "https://i0.hdslb.com/bfs/live/b4a5fe46e7ca9818020a5affaae9eba610f8410d.jpg",
        "title": "原神启程测试在线翻牌",
        "position": 4,
        "link": "https://www.bilibili.com/blackboard/live/activity-wk8Ax2k1k.html"
      },
      {
        "id": 38914,
        "pic": "https://i0.hdslb.com/bfs/live/f6dd6002c6eb07b6a1021d27ada6198881a2a781.jpg",
        "title": "DNF手游主播招募",
        "position": 5,
        "link": "https://www.bilibili.com/blackboard/live/activity-xKTGTsJeQ.html"
      }
    ],
    "tags": [
      {
        "id": 121,
        "name": "综合",
        "sort": "1",
        "sort_type": "sort_type_121"
      },
      {
        "id": 16,
        "name": "最新",
        "sort": "2",
        "sort_type": "live_time"
      },
      {
        "id": 57,
        "name": "萌新",
        "sort": "3",
        "sort_type": "sort_type_57"
      }
    ],
    "new_tags": [
      {
        "id": 121,
        "name": "综合",
        "icon": "",
        "sort_type": "sort_type_121",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 16,
        "name": "最新",
        "icon": "",
        "sort_type": "live_time",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 57,
        "name": "萌新",
        "icon": "",
        "sort_type": "sort_type_57",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~

### 单机

https://api.live.bilibili.com/room/v3/area/getRoomList?platform=web&parent_area_id=6&cate_id=0&area_id=0&sort_type=sort_type_150&page=1&page_size=2&tag_version=1

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 4363,
    "list": [
      {
        "roomid": 5050,
        "uid": 433351,
        "title": "高考加油！但是你还在看直播:/",
        "uname": "EdmundDZhang",
        "online": 1889146,
        "user_cover": "https://i0.hdslb.com/bfs/live/room_cover/17c749332753cb813ef2d0a9d276ceec375907a2.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe07062116000000005050ru7ho5.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/room_cover/17c749332753cb813ef2d0a9d276ceec375907a2.jpg",
        "show_cover": "https://i0.hdslb.com/bfs/live/show_cover/0a96fd538aaf92bc6a5262ad7173087796116f7f.jpg",
        "link": "/5050",
        "face": "https://i0.hdslb.com/bfs/face/5d94b9727a49815716cd66fc7ba3840382025c56.jpg",
        "parent_id": 6,
        "parent_name": "单机",
        "area_id": 236,
        "area_name": "主机游戏",
        "area_v2_parent_id": 6,
        "area_v2_parent_name": "单机",
        "area_v2_id": 236,
        "area_v2_name": "主机游戏",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": {
          "1": {
            "content": "",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/539ce26c45cd4019f55b64cfbcedc3c01820e539.png",
            "position": 1,
            "type": "mobile_index_badge",
            "name": "百人成就"
          }
        },
        "verify": {
          "role": 2,
          "desc": "bilibili直播签约主播",
          "type": 0
        },
        "head_box": {
          "name": "Star星耀",
          "value": "https://i0.hdslb.com/bfs/vc/8b8e483ae46ef634e9babe3c13b9dbb99f374c38.png"
        },
        "head_box_type": 1,
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "B4A41753-2787-9731-E55F-310AE2D332A4",
        "group_id": 0
      },
      {
        "roomid": 1017,
        "uid": 168598,
        "title": "偷走你的心 女神异闻录5皇家版",
        "uname": "逍遥散人",
        "online": 1338549,
        "user_cover": "https://i0.hdslb.com/bfs/live/a888257b5e1a280f4e9728aa859141837ef871dc.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070621160000000010175v6ngf.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/a888257b5e1a280f4e9728aa859141837ef871dc.jpg",
        "show_cover": "",
        "link": "/1017",
        "face": "https://i0.hdslb.com/bfs/face/a22f28e1190fab6f7fa2b81fadae1974cadf02f4.jpg",
        "parent_id": 6,
        "parent_name": "单机",
        "area_id": 283,
        "area_name": "独立游戏",
        "area_v2_parent_id": 6,
        "area_v2_parent_name": "单机",
        "area_v2_id": 283,
        "area_v2_name": "独立游戏",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": {
          "1": {
            "content": "",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/539ce26c45cd4019f55b64cfbcedc3c01820e539.png",
            "position": 1,
            "type": "mobile_index_badge",
            "name": "百人成就"
          }
        },
        "verify": {
          "role": 1,
          "desc": "bilibili 2019百大UP主、直播签约主播",
          "type": 0
        },
        "head_box": {
          "name": "周星star_X头像框",
          "value": "https://i0.hdslb.com/bfs/vc/922eb74a4c5353812450718c93a4cdb624f1dba6.png"
        },
        "head_box_type": 1,
        "is_auto_play": 1,
        "flag": 0,
        "session_id": "531FDA9A-D374-F06E-7F60-05D85FE2655C",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 40163,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 1,
        "link": "https://www.bilibili.com/blackboard/live/activity-tcGTd17UO.html"
      },
      {
        "id": 40195,
        "pic": "https://i0.hdslb.com/bfs/live/668c9aa8ed0645a5bb368bfa5acb75263f32276d.jpg",
        "title": "单机直播白金日历 第一期震撼上线！",
        "position": 3,
        "link": "https://www.bilibili.com/blackboard/live/activity-sc6kgH_Zm.html"
      },
      {
        "id": 40366,
        "pic": "https://i0.hdslb.com/bfs/live/17a9901b1fe91a2a24bf8805a6220cc6b27dfe3d.jpg",
        "title": "萝卜吃米洛生日会",
        "position": 4,
        "link": "https://live.bilibili.com/866466?broadcast_type=0"
      }
    ],
    "tags": [
      {
        "id": 150,
        "name": "综合",
        "sort": "1",
        "sort_type": "sort_type_150"
      },
      {
        "id": 151,
        "name": "最新",
        "sort": "2",
        "sort_type": "live_time"
      },
      {
        "id": 232,
        "name": "星玩家",
        "sort": "3",
        "sort_type": "sort_type_232"
      }
    ],
    "new_tags": [
      {
        "id": 150,
        "name": "综合",
        "icon": "",
        "sort_type": "sort_type_150",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 151,
        "name": "最新",
        "icon": "",
        "sort_type": "live_time",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 232,
        "name": "星玩家",
        "icon": "",
        "sort_type": "sort_type_232",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~

### 娱乐

https://api.live.bilibili.com/room/v3/area/getRoomList?parent_area_id=1&cate_id=0&area_id=0&sort_type=sort_type_152&page=1&page_size=2

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 3147,
    "list": [
      {
        "roomid": 21504767,
        "uid": 886992,
        "title": "婚纱蕾姆COS~",
        "uname": "碳烧奶蛋",
        "online": 204117,
        "user_cover": "https://i0.hdslb.com/bfs/live/user_cover/606ebddc11e4fe4f5ff974e4193f1859a18b102f.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070621170000215047670oyoks.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/user_cover/606ebddc11e4fe4f5ff974e4193f1859a18b102f.jpg",
        "show_cover": "https://i0.hdslb.com/bfs/live/user_cover/c3c998e64f6d161bb6ebd9c0ddfd1f776932e7c2.jpg",
        "link": "/21504767",
        "face": "https://i1.hdslb.com/bfs/face/af0df8822725b83ba3fbe0bdbf174bb995058157.jpg",
        "parent_id": 1,
        "parent_name": "娱乐",
        "area_id": 21,
        "area_name": "视频唱见",
        "area_v2_parent_id": 1,
        "area_v2_parent_name": "娱乐",
        "area_v2_id": 21,
        "area_v2_name": "视频唱见",
        "web_pendent": "hourRank",
        "pk_id": 0,
        "pendant_info": {
          "1": {
            "content": "",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/539ce26c45cd4019f55b64cfbcedc3c01820e539.png",
            "position": 1,
            "type": "mobile_index_badge",
            "name": "百人成就"
          },
          "2": {
            "content": "上小时总榜No.1",
            "color": "#fb7299",
            "pic": "https://i0.hdslb.com/bfs/vc/f1ae40390f1d613735cafd9e45dd07c808d6a866.png",
            "position": 2,
            "type": "mobile_index_badge",
            "name": "last_hour"
          }
        },
        "verify": {
          "role": 0,
          "desc": "",
          "type": -1
        },
        "head_box_type": 1,
        "head_box": {
          "name": "“粽叶飘香”头像框",
          "value": "https://i0.hdslb.com/bfs/vc/da2497540c5dc6a7f555f8d255f8ba2bc5221852.png"
        },
        "is_auto_play": 0,
        "flag": 0,
        "session_id": "1471A05A-52F0-9E1C-3FE0-BC06C5F4F3A1",
        "group_id": 0
      },
      {
        "roomid": 772692,
        "uid": 4605126,
        "title": "小鸟聊天时间",
        "uname": "Misaki阿妹",
        "online": 237167,
        "user_cover": "https://i0.hdslb.com/bfs/live/new_room_cover/d87e76f81369c3a194d1ebf59f1c3c3dfe3fa6e9.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070621170000007726927dq6rg.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/new_room_cover/d87e76f81369c3a194d1ebf59f1c3c3dfe3fa6e9.jpg",
        "show_cover": "https://i0.hdslb.com/bfs/live/user_cover/1eb9ee9cac7b13b6fd510c2b22acaa812acd9db8.jpg",
        "link": "/772692",
        "face": "https://i0.hdslb.com/bfs/face/0bf4533bd0bfe0c2adc5a0cfe44a6f9dbc8b957d.jpg",
        "parent_id": 1,
        "parent_name": "娱乐",
        "area_id": 145,
        "area_name": "视频聊天",
        "area_v2_parent_id": 1,
        "area_v2_parent_name": "娱乐",
        "area_v2_id": 145,
        "area_v2_name": "视频聊天",
        "web_pendent": "lottery_draw_1",
        "pk_id": 0,
        "pendant_info": {
          "2": {
            "content": "正在抽奖",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/d93df3b91780c1124eeacb945bf173236d99623f.png",
            "position": 2,
            "type": "mobile_index_badge",
            "name": "lottery_draw_1"
          }
        },
        "verify": {
          "role": 0,
          "desc": "",
          "type": -1
        },
        "head_box_type": 1,
        "head_box": {
          "name": "“粽情端午”头像框",
          "value": "https://i0.hdslb.com/bfs/vc/ba1929de782fd3d039b497d85ad87038cc83317e.png"
        },
        "is_auto_play": 0,
        "flag": 0,
        "session_id": "8624F995-6970-05C3-F662-04F5B0A7310C",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 39989,
        "pic": "https://i0.hdslb.com/bfs/live/325fa0b7a55d2e965b30161bae2fda0b07063604.png",
        "title": "NEW STAR",
        "position": 1,
        "link": "https://www.bilibili.com/blackboard/live/activity-changjianzhaomu.html"
      },
      {
        "id": 40164,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 2,
        "link": "https://www.bilibili.com/blackboard/live/activity-tcGTd17UO.html"
      },
      {
        "id": 40378,
        "pic": "https://i0.hdslb.com/bfs/live/11a5c75b10f02185937338356a881605ebe9b799.png",
        "title": "MUSICAT首秀",
        "position": 3,
        "link": "https://live.bilibili.com/21242777?broadcast_type=0"
      },
      {
        "id": 40179,
        "pic": "https://i0.hdslb.com/bfs/live/c77d550c9f248af3a65413c9a2186c4a4be1650a.jpg",
        "title": "风雨无阻◎～今天的虚拟世界也是晴空万里～",
        "position": 4,
        "link": "https://www.bilibili.com/blackboard/live/activity-i-xw2n0pA.html"
      },
      {
        "id": 40018,
        "pic": "https://i0.hdslb.com/bfs/live/8e10f92ae0b29fb824f3869be26994442577fe13.jpg",
        "title": "我的舞台我创造！来开播瓜分4w奖金池吧！",
        "position": 5,
        "link": "https://www.bilibili.com/blackboard/live/activity-wdwtwcz.html"
      }
    ],
    "tags": [
      {
        "id": 152,
        "name": "推荐",
        "sort": "1",
        "sort_type": "sort_type_152"
      },
      {
        "id": 224,
        "name": "最热",
        "sort": "2",
        "sort_type": "online"
      },
      {
        "id": 153,
        "name": "萌新",
        "sort": "3",
        "sort_type": "sort_type_153"
      }
    ],
    "new_tags": [
      {
        "id": 152,
        "name": "推荐",
        "icon": "",
        "sort_type": "sort_type_152",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 224,
        "name": "最热",
        "icon": "",
        "sort_type": "online",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 153,
        "name": "萌新",
        "icon": "",
        "sort_type": "sort_type_153",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~

### 电台

https://api.live.bilibili.com/room/v3/area/getRoomList?platform=web&parent_area_id=5&cate_id=0&area_id=0&sort_type=sort_type_226&page=1&page_size=30&tag_version=1

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 667,
    "list": [
      {
        "roomid": 623698,
        "uid": 16510117,
        "title": "第4天直播 会有人来听我唱歌吗",
        "uname": "机智大灯灯",
        "online": 37885,
        "user_cover": "https://i0.hdslb.com/bfs/live/new_room_cover/3efffa7ad93ce837e9791bdac2c5b11372677383.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070621210000006236981go1z0.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/new_room_cover/3efffa7ad93ce837e9791bdac2c5b11372677383.jpg",
        "show_cover": "",
        "link": "/623698",
        "face": "https://i0.hdslb.com/bfs/face/ed4c6255026138531b4910325fcaa5d4f5df6d9d.jpg",
        "parent_id": 5,
        "parent_name": "电台",
        "area_id": 190,
        "area_name": "唱见电台",
        "area_v2_parent_id": 5,
        "area_v2_parent_name": "电台",
        "area_v2_id": 190,
        "area_v2_name": "唱见电台",
        "web_pendent": "last_area_hour_5",
        "pk_id": 0,
        "pendant_info": {
          "2": {
            "content": "上小时电台No.1",
            "color": "#fb7299",
            "pic": "https://i0.hdslb.com/bfs/vc/f1ae40390f1d613735cafd9e45dd07c808d6a866.png",
            "position": 2,
            "type": "mobile_index_badge",
            "name": "last_area_hour_5"
          }
        },
        "verify": {
          "role": 0,
          "desc": "",
          "type": -1
        },
        "is_auto_play": 0,
        "flag": 0,
        "session_id": "E0773E4B-A91F-F392-4ECB-5710974DADE4",
        "group_id": 0
      },
      {
        "roomid": 266481,
        "uid": 14096411,
        "title": "你宁愿错过我 也不愿意主动嘛",
        "uname": "撩人小姐姐",
        "online": 24443,
        "user_cover": "https://i0.hdslb.com/bfs/live/user_cover/780a49e94977b219a8b0734b78b28b1d760fe29b.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe07062121000000266481xeemn5.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/user_cover/780a49e94977b219a8b0734b78b28b1d760fe29b.jpg",
        "show_cover": "",
        "link": "/266481",
        "face": "https://i2.hdslb.com/bfs/face/892b39876f7cae165bba9b455566b3429508c289.jpg",
        "parent_id": 5,
        "parent_name": "电台",
        "area_id": 192,
        "area_name": "聊天电台",
        "area_v2_parent_id": 5,
        "area_v2_parent_name": "电台",
        "area_v2_id": 192,
        "area_v2_name": "聊天电台",
        "web_pendent": "lottery_draw_2",
        "pk_id": 0,
        "pendant_info": {
          "2": {
            "content": "正在抽奖",
            "color": "",
            "pic": "https://i0.hdslb.com/bfs/live/d93df3b91780c1124eeacb945bf173236d99623f.png",
            "position": 2,
            "type": "mobile_index_badge",
            "name": "lottery_draw_2"
          }
        },
        "verify": {
          "role": 2,
          "desc": "bilibili直播签约主播",
          "type": 0
        },
        "head_box": {
          "name": "“粽情端午”头像框",
          "value": "https://i0.hdslb.com/bfs/vc/ba1929de782fd3d039b497d85ad87038cc83317e.png"
        },
        "head_box_type": 1,
        "is_auto_play": 0,
        "flag": 0,
        "session_id": "70D9F660-78BF-DB18-D1AE-56B7547F3626",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 40180,
        "pic": "https://i0.hdslb.com/bfs/live/9adf8b961e7946376dcd179e84cff5d4ea44b421.png",
        "title": "大阿酥er",
        "position": 1,
        "link": "https://live.bilibili.com/22100272?broadcast_type=0"
      },
      {
        "id": 40165,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 2,
        "link": "https://www.bilibili.com/blackboard/live/activity-tcGTd17UO.html"
      },
      {
        "id": 40254,
        "pic": "https://i0.hdslb.com/bfs/live/ee8d277759aae2c11c84f4ccd31b4e28ce127107.png",
        "title": "七月子不语——开播解锁丰厚资源",
        "position": 3,
        "link": "https://www.bilibili.com/blackboard/live/Julyghost_PC.html"
      },
      {
        "id": 39940,
        "pic": "https://i0.hdslb.com/bfs/live/ce2ff5ca49cc58d930013034af287f0f95af7cef.jpg",
        "title": " 一周热门电台唱见",
        "position": 4,
        "link": "https://www.bilibili.com/blackboard/live/activity-remenchangjian.html"
      },
      {
        "id": 39977,
        "pic": "https://i0.hdslb.com/bfs/live/c14199d9c8862a783c28fa0105f62e5148d5eb04.jpg",
        "title": "你的歌会我承包啦",
        "position": 5,
        "link": "https://www.bilibili.com/blackboard/live/activity-singerlive.html"
      }
    ],
    "tags": [
      {
        "id": 226,
        "name": "推荐",
        "sort": "1",
        "sort_type": "sort_type_226"
      },
      {
        "id": 8,
        "name": "热门",
        "sort": "2",
        "sort_type": "sort_type_8"
      },
      {
        "id": 17,
        "name": "萌新",
        "sort": "3",
        "sort_type": "sort_type_17"
      }
    ],
    "new_tags": [
      {
        "id": 226,
        "name": "推荐",
        "icon": "",
        "sort_type": "sort_type_226",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 8,
        "name": "热门",
        "icon": "",
        "sort_type": "sort_type_8",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 17,
        "name": "萌新",
        "icon": "",
        "sort_type": "sort_type_17",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~

### 绘画

https://api.live.bilibili.com/room/v3/area/getRoomList?platform=web&parent_area_id=4&cate_id=0&area_id=0&sort_type=sort_type_56&page=1&page_size=30&tag_version=1

~~~json
{
  "code": 0,
  "msg": "success",
  "message": "success",
  "data": {
    "count": 511,
    "list": [
      {
        "roomid": 4829275,
        "uid": 24259775,
        "title": "场景绘制",
        "uname": "行之LV",
        "online": 26319,
        "user_cover": "https://i0.hdslb.com/bfs/live/room_cover/e18a363d9eb8fd2c17328e2aa16701b6ac0c5680.jpg",
        "user_cover_flag": 1,
        "system_cover": "https://i0.hdslb.com/bfs/live/keyframe070621220000048292753tq33k.jpg",
        "cover": "https://i0.hdslb.com/bfs/live/room_cover/e18a363d9eb8fd2c17328e2aa16701b6ac0c5680.jpg",
        "show_cover": "",
        "link": "/4829275",
        "face": "https://i1.hdslb.com/bfs/face/8cbae95c1fd6fe92c7938395c3a249e425202460.jpg",
        "parent_id": 4,
        "parent_name": "绘画",
        "area_id": 51,
        "area_name": "原创绘画",
        "area_v2_parent_id": 4,
        "area_v2_parent_name": "绘画",
        "area_v2_id": 51,
        "area_v2_name": "原创绘画",
        "web_pendent": "",
        "pk_id": 0,
        "pendant_info": [
          
        ],
        "verify": {
          "role": 0,
          "desc": "",
          "type": -1
        },
        "head_box_type": 2,
        "head_box": {
          "name": "bilibili秋",
          "value": "https://i2.hdslb.com/bfs/face/aa04bf5ba7157461de99ab4de2e6250b3498d304.png"
        },
        "is_auto_play": 0,
        "flag": 0,
        "session_id": "579C225E-DC10-A87C-42B3-571901F66E9C",
        "group_id": 0
      }
    ],
    "banner": [
      {
        "id": 40133,
        "pic": "https://i0.hdslb.com/bfs/live/06821bb602fa56ef8fa9a39e95e96494e0c7a4f6.jpg",
        "title": "公会招募扶持计划，千万奖励重磅来袭！",
        "position": 2,
        "link": "https://www.bilibili.com/blackboard/live/activity-5fKbx_wZC.html"
      }
    ],
    "tags": [
      {
        "id": 56,
        "name": "综合",
        "sort": "1",
        "sort_type": "sort_type_56"
      },
      {
        "id": 59,
        "name": "最新",
        "sort": "2",
        "sort_type": "live_time"
      },
      {
        "id": 60,
        "name": "萌新",
        "sort": "3",
        "sort_type": "sort_type_60"
      }
    ],
    "new_tags": [
      {
        "id": 56,
        "name": "综合",
        "icon": "",
        "sort_type": "sort_type_56",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "1"
      },
      {
        "id": 59,
        "name": "最新",
        "icon": "",
        "sort_type": "live_time",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "2"
      },
      {
        "id": 60,
        "name": "萌新",
        "icon": "",
        "sort_type": "sort_type_60",
        "type": 0,
        "sub": [
          
        ],
        "hero_list": [
          
        ],
        "sort": "3"
      }
    ]
  }
}
~~~



## 消息推送（钉钉机器人）

https://oapi.dingtalk.com/robot/send?access_token=08b7b4e629624423f37058158c85d3809029f10eef0807ee0c9a3c585a79a006

~~~cmd
curl 'https://oapi.dingtalk.com/robot/sendaccess_token=08b7b4e629624423f37058158c85d3809029f10eef0807ee0c9a3c585a79a006' \ -H 'Content-Type: application/json' \ -d '{"msgtype": "text", "text": { "content": "本条服务器消息为测试消息，关键词为服务器测试" } }' 
~~~



主播人气排行榜

主播小时内合计人气/小分区内小时总人气

小分区小时开播总数量    

大分区小时开播总数量 = 小分区小时开播总数量 + 小分区小时开播总数量 + ...

小分区小时开播总数量/大分区小时开播总数量



