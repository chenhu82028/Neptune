package com.lathingfisher.neptune.controller;

import com.alibaba.fastjson.JSONObject;
import com.lathingfisher.neptune.dao.NeptuneMapper;
import com.lathingfisher.neptune.service.NeptuneLog;
import com.lathingfisher.neptune.service.NeptuneService;
import com.lathingfisher.neptune.util.HttpUtil;
import com.lathingfisher.neptune.util.IPLocation;
import com.lathingfisher.neptune.util.Location;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author C.Hu
 * @version 1.0
 * @date 2020/1/15 15:49
 */
@Controller
@SuppressWarnings("all")
@Component
public class neptuneController {

    @Autowired
    public NeptuneService neptuneService;

    @Autowired
    public NeptuneLog neptuneLog;

    @Autowired
    public NeptuneMapper neptuneMapper;

    @Value("${intervals_time}")
    public Integer intervals;

    public static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static Pattern pattern = Pattern.compile("[^0-9]");

    @RequestMapping("/neptune")
    @ResponseBody
    public Map<String, Object> neptune(String type, String value, HttpServletRequest request) {

        Map<String, Object> resultData = new HashMap<>(2);
        List<Map<String, Object>> mapList = null;

        /**
         * 1:海王查询
         * 2:勋章查询
         * 3:房管查询
         * 4:友爱社查询
         */
        try {

            String uid = null;
            if ("1".equals(type)) {
                //海王查询
                if (inquireNavigation(type, value, request, resultData, mapList, uid)) return resultData;

            } else if ("2".equals(type)) {

                if ("".equals(value.trim())) {
                    mapList = neptuneService.searchAllModelRank();
                } else {
                    mapList = neptuneService.searchModelRank(value.trim());
                }
                resultData.put("result", "success");
                resultData.put("data", mapList);
                resultData.put("type", 2);
                saveLog(type, value, request, "success");

            } else if ("3".equals(type)) {

                if (value.contains("UID") || value.contains("uid")) {

                    uid = getUidToUID(value, uid);

                } else if ("".equals(value.trim())) {
                    resultData.put("data", mapList);
                    resultData.put("result", "success");
                    resultData.put("type", 1);
                    return resultData;
                } else {

                    uid = geUid(value);
                }

                if (uid == null) {
                    resultData.put("result", "error");
                    saveLog(type, value, request, "error");
                    return resultData;
                }

                if ("拜托了_喵大人".equals(value) || "301507852".equals(uid)) {
                    resultData.put("result", "errormsg");
                    resultData.put("data", "房管是什么，我都不知道");
                    saveLog(type, value, request, "error");
                    return resultData;
                }

                mapList = neptuneService.selectRoomAdmin(uid);
                resultData.put("data", mapList);
                resultData.put("result", "success");
                resultData.put("type", 3);
                saveLog(type, value, request, "success");
            }

        } catch (Exception e) {
            e.printStackTrace();
            resultData.put("result", "error");
        }

        return resultData;
    }

    private boolean inquireNavigation(String type, String value, HttpServletRequest request, Map<String, Object> resultData, List<Map<String, Object>> mapList, String uid) throws IOException {
        if (value.contains("UID") || value.contains("uid")) {

            uid = getUidToUID(value, uid);

        } else if ("".equals(value.trim())) {
            resultData.put("data", mapList);
            resultData.put("result", "success");
            resultData.put("type", 1);
            return true;
        } else {

            uid = geUid(value);
        }

        if (uid == null) {
            resultData.put("result", "errormsg");
            resultData.put("data", "没找到这个用户呢，建议用UID查询,如（UID:123456）");
            saveLog(type, value, request, "error");
            return true;
        }

        if ("拜托了_喵大人".equals(value) || "301507852".equals(uid)) {
            resultData.put("result", "errormsg");
            resultData.put("data", "哎呀~~~别看了,我真的一条都没有");
            saveLog(type, value, request, "error");
            return true;
        }

        mapList = neptuneService.selectSearch(uid);

        if (mapList.size() == 0) {

            resultData.put("result", "errormsg");
            resultData.put("data", "没查到这个人的船呢");
        } else {
            resultData.put("data", mapList);
            resultData.put("result", "success");
        }

        resultData.put("type", 1);
        saveLog(type, value, request, "success");
        return false;
    }


    //    @RequestMapping("/testNeptune")
//    @Scheduled(cron = "0 */60 * * * ?")
//    @Scheduled(initialDelay = 1000, fixedRate = )
    public void timedTask() {

        List<Map<String, Object>> maps = new ArrayList<>();
        List<Map<String, Object>> anchor = new ArrayList<>();
        long start = System.currentTimeMillis();
        System.out.println(sf.format(new Date()));

        try {

            //获取全部直播的总数
            JSONObject result = HttpUtil.getResult("https://api.live.bilibili.com/room/v1/Area/getLiveRoomCountByAreaID?areaId=0");
            Object data = result.get("data");
            Map<String, Integer> data1 = (Map) data;
            Integer totalNum = data1.get("num");

            JSONObject result1;
            List<Map<String, Object>> dataList;

            Object msg;

            //计算出总共多少页
            double ceil = Math.ceil(Double.valueOf(totalNum) / 100);

            //循环遍历
            for (int i = 0; i <= 0; i++) {

                result1 = HttpUtil.getResult("https://api.live.bilibili.com/room/v1/room/get_user_recommend?page_size=100&page=" + i);

                msg = result1.get("msg");

                if (msg != null) {

                    if ("ok".equals(msg.toString())) {

                        List<Map<String, Object>> data2 = (List<Map<String, Object>>) result1.get("data");

                        if (data2.size() > 0) {

                            maps.addAll(data2);
                        }

                    }
                }

                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Object roomid;
            Object uid;
            JSONObject guardTab;

            List<Map<String, Object>> insertMaps = new ArrayList<>();

            long mind = System.currentTimeMillis();

            System.out.println("获取总的房间数用时：" + (mind - start) + ",房间总数为：" + maps.size());

            for (Map<String, Object> map : maps) {
                roomid = map.get("roomid");
                uid = map.get("uid");
                if (roomid != null && uid != null) {

                    //舰长
//                    getGuardTab(roomid, uid);

                    //获取主播信息
//                    getAnchor(roomid, uid);
                    getAnchor(roomid, anchor);

                    //友爱社
//                    getUnionFans(roomid, uid);

                    //房管
//                    getRoomAdmin(roomid, uid);

                    //粉丝榜
//                    getMedalRank(roomid, uid);

                }
            }

            System.out.println(sf.format(new Date()));

            Map<Integer, List<Map<String, Object>>> integerListMap = batchList(anchor, 500);

            for (List<Map<String, Object>> value : integerListMap.values()) {
                neptuneService.insertNewAnchorListUser(value);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("总共：" + maps.size() + "条数据，全部用时:" + (System.currentTimeMillis() - start) / 1000);
    }

    @Scheduled(initialDelay = 1000, fixedRate = 480000)
    public void aaa() throws IOException {

        int parent_area_id = 1;
        System.out.println(sf.format(new Date()));
        long start = System.currentTimeMillis();

        List<Map<String, Object>> entertainment = getResult(1);//娱乐
        List<Map<String, Object>> onlineGames = getResult(2);//网游
        List<Map<String, Object>> mobileGames = getResult(3);//手游
        List<Map<String, Object>> painting = getResult(4);//绘画
        List<Map<String, Object>> radioStation = getResult(5);//电台
        List<Map<String, Object>> standAlone = getResult(6);//单机

        entertainment.addAll(onlineGames);
        entertainment.addAll(mobileGames);
        entertainment.addAll(painting);
        entertainment.addAll(radioStation);
        entertainment.addAll(standAlone);

        Map<Integer, List<Map<String, Object>>> integerListMap = batchList(entertainment, 500);

        for (List<Map<String, Object>> value : integerListMap.values()) {
            neptuneService.insertNewAnchorListUser2(value);
        }

        System.out.println((System.currentTimeMillis() - start)/1000 + "，总数为：" + entertainment.size());

    }

    private List<Map<String, Object>> getResult(int parent_area_id) throws IOException {
        List<Map<String, Object>> list = null;
        JSONObject result = HttpUtil.getResult("https://api.live.bilibili.com/room/v3/area/getRoomList?parent_area_id="+parent_area_id+"&cate_id=0&area_id=0&page_size=90&page=0");// 获取连接
        Object msg = result.get("msg");
        if (msg != null && msg.toString().equals("success")) {

            Map<String, Object> data = (Map<String, Object>) result.get("data");
            Integer count = (Integer) data.get("count");
            list = (List<Map<String, Object>>) data.get("list");

            double ceil = Math.ceil(Double.valueOf(count) / 90);

            for (int i = 1; i < ceil; i++) {
                result = HttpUtil.getResult("https://api.live.bilibili.com/room/v3/area/getRoomList?parent_area_id="+parent_area_id+"&cate_id=0&area_id=0&page_size=90&page="+ i);// 获取连接
                msg = result.get("msg");
                if (msg != null && msg.toString().equals("success")) {
                    data = (Map<String, Object>) result.get("data");
                    list.addAll((List<Map<String, Object>>) data.get("list"));
                }
            }

        }
        return list;
    }

    public Map<Integer, List<Map<String, Object>>> batchList(List<Map<String, Object>> list, int batchSize) {
        Map<Integer, List<Map<String, Object>>> itemMap = new HashMap<>();
        itemMap.put(1, new ArrayList<Map<String, Object>>());
        for (Map e : list) {
            List<Map<String, Object>> batchList = itemMap.get(itemMap.size());
            if (batchList.size() == batchSize) {//当list满足批次数量，新建一个list存放后面的数据
                batchList = new ArrayList<Map<String, Object>>();
                itemMap.put(itemMap.size() + 1, batchList);
            }
            batchList.add(e);
        }
        return itemMap;
    }

    private String getUidToUID(String value, String uid) {
        try {
            Matcher matcher = pattern.matcher(value);
            String trim = matcher.replaceAll("").trim();
            if (trim != null && !"".equals(trim)) {
                uid = trim;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return uid;
    }

    /**
     * 保存查询结果
     *
     * @param type
     * @param value
     * @param request
     * @param result
     */
    private void saveLog(String type, String value, HttpServletRequest request, String result) {
        try {
            String requestURI = request.getRemoteAddr();
            String addresses = "";

            if (requestURI != null && !"".equals(requestURI)) {
                IPLocation ipLocation = new IPLocation("/www/wwwroot/default/qqwry.dat");
                Location location = ipLocation.fetchIPLocation(requestURI);
                if (location != null) {
                    addresses = location.toString();
                }
            }

            neptuneLog.inertSearchLog(requestURI, type, value, addresses, result);

        } catch (Exception e) {

        }
    }

//    private void getMedalRank(Object roomid, Object uid) throws IOException {
//        JSONObject medalRank = HttpUtil.getResult("https://api.live.bilibili.com/rankdb/v1/RoomRank/webMedalRank?roomid=" + roomid + "&ruid=" + uid);
//        JSONObject dataMedalRank = (JSONObject) medalRank.get("data");
//        List<Map<String, Object>> list = (List) dataMedalRank.get("list");
//
//        if (list.size() > 0) {
//
//            for (Map<String, Object> stringObjectMap : list) {
//
//                stringObjectMap.put("roomid", roomid);
//                stringObjectMap.put("ruid", uid);
//
//            }
//
//            List<Map<String, Object>> mapList = neptuneService.selectByMedalRank(roomid);
//            if (mapList.size() > 0) {
//
//                List<Map<String, Object>> mapList1 = compareListDataNotInInsert(list, mapList);
//                if (mapList1.size() > 0) {
//
//                    neptuneService.insertMedalRank(mapList1);
////                    System.out.println("进入一批：");
//                }
//
//                List<Long> integers = compareListDataNotInDelete(list, mapList);
//                if (integers.size() > 0) {
//
//                    neptuneService.deleteMedalRank(integers);
//                }
//
//
//            } else {
//
//                neptuneService.insertMedalRank(list);
////                System.out.println("进入一批：");
//            }
//
//        }
//    }

    /**
     * 详情版
     *
     * @param roomid
     * @throws IOException
     */
    private void getAnchor(Object roomid, List<Map<String, Object>> list) throws IOException {

        try {
            Thread.sleep(intervals);

            JSONObject anchorMassage = HttpUtil.getResult("https://api.live.bilibili.com/xlive/web-room/v1/index/getInfoByRoom?room_id=" + roomid);
            Map<String, Object> anchordata = (Map) anchorMassage.get("data");
            Map<String, Object> room_info = (Map) anchordata.get("room_info");
            Map<String, Object> anchor_info = (Map) anchordata.get("anchor_info");
            Map<String, Object> base_info = (Map) anchor_info.get("base_info");


            Map<String, Object> map = new HashMap<>();
            map.put("uname", base_info.get("uname"));//uname -> Dr幻梦
            map.put("room_id", room_info.get("room_id"));//room_id -> {Integer@7127}3687965
            map.put("area_name", room_info.get("area_name"));//area_name -> 唱见电台
            map.put("area_id", room_info.get("area_id"));//area_id -> {Integer@7136}190
            map.put("uid", room_info.get("uid"));//uid -> {Integer@7140}74429905
            map.put("parent_area_id", room_info.get("parent_area_id"));//parent_area_id -> {Integer@7145}5
            map.put("parent_area_name", room_info.get("parent_area_name"));//parent_area_name -> 电台
            map.put("online", room_info.get("online"));//online -> {Integer@7666}117 人气

            Base64 base64 = new Base64();
            String base64Sign = base64.encodeToString(room_info.get("title").toString().getBytes("UTF-8"));
            map.put("title", base64Sign);//title -> 第6天 你的同桌/他律自习室/21考研  标题

            map.put("adddate", sf.format(new Date()));//采集到的时间

//            neptuneService.insertNewAnchorListUser(list);
            list.add(map);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    /**
//     * 基础信息版
//     *
//     * @param roomid
//     * @param uid
//     * @throws IOException
//     */
//    private void getAnchor(Object roomid, Object uid) throws IOException {
//
//        try {
//            JSONObject anchorMassage = HttpUtil.getResult("https://api.live.bilibili.com/room_ex/v1/RoomNews/get?roomid=" + roomid + "&uid=" + uid);
//            Map<String, Object> anchordata = (Map) anchorMassage.get("data");
//            if (anchordata != null) {
//                if (anchordata.size() > 0) {
//
//                    Object anchorRoomid = anchordata.get("roomid");
//
//                    Object anchorUname = anchordata.get("uname");
//                    Object anchorUid = anchordata.get("uid");
//                    if (anchorUid != null) {
//                        String string = String.valueOf(anchorUid);
//                        if ("".equals(string)) {
//                            anchorUid = uid;
//                        }
//                    }
//                    neptuneService.insertAnchorUser(anchorRoomid, anchorUname, anchorUid);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private List<Map<String, Object>> getRoomAdmin(Object roomid, Object uid) throws IOException {
        JSONObject roomAdmin = HttpUtil.getResult("https://api.live.bilibili.com/xlive/web-room/v1/roomAdmin/get_by_room?roomid=" + roomid + "&page_size=100");
        JSONObject dataRoomAdmin = (JSONObject) roomAdmin.get("data");
        List<Map<String, Object>> data2 = (List) dataRoomAdmin.get("data");
        if (data2.size() > 0) {

            for (Map<String, Object> stringObjectMap : data2) {
                stringObjectMap.put("roomid", roomid);
                stringObjectMap.put("ruid", uid);
            }

            List<Map<String, Object>> mapList = neptuneService.selectByRoomAdmin(uid);

            if (mapList.size() > 0) {


                List<Map<String, Object>> mapList1 = compareListDataNotInInsert(data2, mapList);
                if (mapList1.size() > 0) {

                    neptuneService.insertRoomAdmin(mapList1);
                }

                List<Long> integers = compareListDataNotInDelete(data2, mapList);
                if (integers.size() > 0) {

                    neptuneService.deleteRoomAdmin(integers);
                }

            } else {
                neptuneService.insertRoomAdmin(data2);
            }
        }
        return data2;
    }

    private void getUnionFans(Object roomid, Object uid) throws IOException {
        JSONObject UnionFans = HttpUtil.getResult("https://api.live.bilibili.com/activity/v1/UnionFans/roomFansRank?roomid=" + roomid + "&ruid=" + uid);
        List<Map<String, Object>> list = (List) UnionFans.get("data");
        if (list.size() > 0) {

            for (Map<String, Object> stringObjectMap : list) {

                stringObjectMap.put("roomid", roomid);
                stringObjectMap.put("ruid", uid);

                neptuneService.insertUnionFans(stringObjectMap);

            }

        }
    }

    private void getGuardTab(Object roomid, Object uid) throws IOException {
        JSONObject guardTab;
        JSONObject dataGuardTab;
        List<Map<String, Object>> top3;
        List<Map<String, Object>> list;
        Map<String, Object> info;
        JSONObject guardTab1;
        List<Map<String, Object>> data2;

        //舰队
        guardTab = HttpUtil.getResult("https://api.live.bilibili.com/xlive/app-room/v1/guardTab/topList?roomid=" + roomid + "&page=1&ruid=" + uid + "&page_size=29");

        dataGuardTab = (JSONObject) guardTab.get("data");

        top3 = (List) dataGuardTab.get("top3");

        if (top3.size() > 0) {

            list = (List) dataGuardTab.get("list");
            info = (Map) dataGuardTab.get("info");

            if (list.size() > 0) {
                top3.addAll(list);

                if (info != null) {
                    Integer num = (Integer) info.get("num");
                    if (num > 0) {

                        Integer integer = (Integer) info.get("page");

                        for (int i = 2; i <= integer; i++) {

                            guardTab1 = HttpUtil.getResult("https://api.live.bilibili.com/xlive/app-room/v1/guardTab/topList?roomid=" + roomid + "&page=" + i + "&ruid=" + uid + "&page_size=29");
                            data2 = HttpUtil.getData(guardTab1);
                            if (data2.size() > 0) {
                                top3.addAll(data2);
                            }
                        }
                    }
                }
            }

            List<Map<String, Object>> mapList = neptuneService.selectByGuardTabRuid(uid);

            if (mapList.size() > 0) {

                List<Map<String, Object>> lists = compareListDataNotInInsert(top3, mapList);

                if (lists.size() > 0) {

                    for (Map<String, Object> stringObjectMap : lists) {
                        stringObjectMap.put("roomid", roomid);
                    }

                    neptuneService.insertGuardTab(lists);
                }

                //更新舰长等级
                compareListDataNotInInsertCheck(top3, mapList);


                List<Long> integers = compareListDataNotInDelete(top3, mapList);

                if (integers.size() > 0) {

                    neptuneService.deleteGuardTab(integers);
                }

            } else {

                for (Map<String, Object> stringObjectMap : top3) {
                    stringObjectMap.put("roomid", roomid);
                }

                neptuneService.insertGuardTab(top3);
            }
        }
    }

    private String geUid(String uname) throws IOException {
        Connection con = Jsoup.connect("http://search.bilibili.com/upuser?keyword=" + uname);// 获取连接

        Response rs = con.execute();// 获取响应

        Document d1 = Jsoup.parse(rs.body());// 转换为Dom树

        String integer = null;
        List<Element> et = d1.select("a.face-img");

        if (et.size() > 0) {

            Element element = et.get(0);
            String href = element.attr("href");

            if (href != null) {
                Matcher matcher = pattern.matcher(href);
                integer = matcher.replaceAll("").trim();
            }

        }

        return integer;
    }

    private void sevenRank(Object roomid, Object uid) {
        try {

            //七日榜
            JSONObject sevenRank = HttpUtil.getResult("https://api.live.bilibili.com/rankdb/v1/RoomRank/webSevenRank?roomid=" + roomid + "&ruid=" + uid);
            JSONObject object = (JSONObject) sevenRank.get("data");
            List<Map<String, Object>> list = (List) object.get("list");
            if (list.size() > 0) {

            }

        } catch (Exception e) {

        }
    }

    /**
     * @param newGuardTab
     * @param oldGuardTab
     */
    public List<Map<String, Object>> compareListDataNotInInsert(List<Map<String, Object>> newGuardTab, List<Map<String, Object>> oldGuardTab) {
        List<Map<String, Object>> lists = new ArrayList<>();
        try {

            boolean b = true;

            for (Map<String, Object> one : newGuardTab) {

                int uidone = (int) one.get("uid");
                top:
                for (Map<String, Object> two : oldGuardTab) {

                    long uidtwo = (long) two.get("UID");

                    if (uidone == uidtwo) {
                        b = false;
                        break top;
                    }
                }

                if (b) {
                    lists.add(one);
                }
                b = true;
            }


        } catch (Exception e) {
            e.getMessage();
        }

        return lists;
    }

    /**
     * @param newGuardTab
     * @param oldGuardTab
     */
    public List<Map<String, Object>> compareListDataNotInInsertCheck(List<Map<String, Object>> newGuardTab, List<Map<String, Object>> oldGuardTab) {
        List<Map<String, Object>> lists = new ArrayList<>();
        String face;
        String username;
        String FACE;
        String USERNAME;

        try {

            for (Map<String, Object> one : newGuardTab) {

                int uidone = (int) one.get("uid");
                int guard_level = (int) one.get("guard_level");
                face = (String) one.get("face");
                username = (String) one.get("username");

                top:
                for (Map<String, Object> two : oldGuardTab) {

                    long uidtwo = (long) two.get("UID");
                    int GUARD_LEVEL = (int) two.get("GUARD_LEVEL");
                    FACE = (String) two.get("FACE");
                    USERNAME = (String) two.get("USERNAME");

                    if (uidone == uidtwo) {

                        if (guard_level != GUARD_LEVEL || !face.equals(FACE) || !username.equals(USERNAME)) {
                            one.put("id", two.get("ID"));
                            lists.add(one);
                            break top;
                        }
                    }
                }
            }

            if (lists.size() > 0) {

                for (Map<String, Object> list : lists) {

                    neptuneService.updateGuardTab(list);
                }

            }
        } catch (Exception e) {
            e.getMessage();
        }

        return lists;
    }

    /**
     * @param newGuardTab
     * @param oldGuardTab
     */
    public List<Long> compareListDataNotInDelete(List<Map<String, Object>> newGuardTab, List<Map<String, Object>> oldGuardTab) {
        List<Long> lists = new ArrayList<>();
        try {

            boolean b = true;
            for (Map<String, Object> one : oldGuardTab) {

                long uidone = (long) one.get("UID");

                top:
                for (Map<String, Object> two : newGuardTab) {

                    int uidtwo = (int) two.get("uid");

                    if (uidone == uidtwo) {
                        b = false;
                        break top;
                    }
                }

                if (b) {
                    lists.add((long) one.get("ID"));
                }

                b = true;
            }


        } catch (Exception e) {
            e.getMessage();
        }

        return lists;
    }

    @RequestMapping("/brush")
    @ResponseBody
    public List<Map> resultBrush() {
        List<Map> topics = neptuneMapper.selectTenTopic();
        System.out.println(111);
        return topics;
    }
}
